package com.zhou.javakc.common.entity.base.restful;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-08 14:35
 */
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResultSets<T> {
    /**
     * 返回结果集
     */
    private T data;
    /**
     * 执行状态码
     */
    private int status;
    /**
     * 提示用户消息
     */
    private String message;

}
