package com.zhou.javakc.common.entity.base.restful;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-08 14:47
 */
public class ResultBuilder {

    /**
     * 执行成功
     * @param code 状态码
     * @param <T> 结果集类型
     * @return 无数据
     */
    public static <T> ResultSets<T> success(ResultCode code){
        ResultSets<T> result = new ResultSets<>();
        result.setStatus(code.getStatus());
        result.setMessage(code.getMessage());
        return result;
    }

    /**
     * 执行成功
     * @param data 返回结果
     * @param code 状态码
     * @param <T> 结果集类型
     * @return 返回数据
     */
    public static <T> ResultSets<T> success(ResultCode code, T data){
        ResultSets<T> result = new ResultSets<>();
        result.setStatus(code.getStatus());
        result.setMessage(code.getMessage());
        result.setData(data);
        return result;
    }

    /**
     * 执行失败
     * @param code 状态码
     * @param <T> 结果集类型
     * @return 无数据
     */
    public static <T> ResultSets<T> error(ResultCode code){
        ResultSets<T> result = new ResultSets<>();
        result.setStatus(code.getStatus());
        result.setMessage(code.getMessage());
        return result;
    }

}
