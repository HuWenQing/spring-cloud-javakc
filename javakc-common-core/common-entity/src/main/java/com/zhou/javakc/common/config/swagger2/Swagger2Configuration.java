package com.zhou.javakc.common.config.swagger2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-09-04 15:15
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {

    private final static String BASE_PACKAGE =
            "com.zhou.javakc.system.user.controller";

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zhou.javakc.system.user.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Cloud Boot中使用Swagger2构建RESTful APIs")
                .description("更多Spring Boot相关文章请关注: http://www.javakc.com/")
                .termsOfServiceUrl("http://www.javakc.com/")
                .version("1.0")
                .build();
    }

}
