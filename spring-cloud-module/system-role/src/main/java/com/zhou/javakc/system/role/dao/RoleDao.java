package com.zhou.javakc.system.role.dao;

import com.zhou.javakc.common.entity.system.Role;
import com.zhou.javakc.common.jpa.base.BaseDao;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-09-06 09:01
 */
public interface RoleDao extends BaseDao<Role, String> {
}
