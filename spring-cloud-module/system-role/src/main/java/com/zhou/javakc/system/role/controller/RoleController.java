package com.zhou.javakc.system.role.controller;

import com.zhou.javakc.common.entity.base.restful.ResultBuilder;
import com.zhou.javakc.common.entity.base.restful.ResultCode;
import com.zhou.javakc.common.entity.base.restful.ResultSets;
import com.zhou.javakc.common.entity.system.Role;
import com.zhou.javakc.common.entity.system.User;
import com.zhou.javakc.system.role.service.RoleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-09-06 09:06
 */
@RestController
@RequestMapping("system")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @ApiOperation(value="展示角色", notes="分页查询角色列表")
    @ApiImplicitParam(name = "entity", value = "角色详细实体Role", required = true, dataType = "Role")
    @PostMapping("role/query")
    public ResultSets<Page<Role>> query(@RequestBody Role entity)
    {
        return ResultBuilder.success(ResultCode.SUCCESS, roleService.findAll(entity));
    }

    @ApiOperation(value="展示角色", notes="获取全部角色列表")
    @GetMapping("role/query")
    public ResultSets<List<Role>> query()
    {
        return ResultBuilder.success(ResultCode.SUCCESS, roleService.findAll());
    }

    @ApiOperation(value="添加角色", notes="根据Role对象创建角色")
    @ApiImplicitParam(name = "entity", value = "角色详细实体Role", required = true, dataType = "Role")
    @PostMapping("role")
    public ResultSets<Role> save(@RequestBody Role entity)
    {
        return ResultBuilder.success(ResultCode.SUCCESS, roleService.save(entity));
    }

    @ApiOperation(value="获取角色", notes="根据请求参数的roleId来获取角色详细信息")
    @ApiImplicitParam(name = "roleId", value = "角色主键ID", required = true, dataType = "String")
    @GetMapping("role/{roleId}")
    public ResultSets<Role> load(@PathVariable String roleId)
    {
        return ResultBuilder.success(ResultCode.SUCCESS, roleService.get(roleId));
    }

    @ApiOperation(value="删除角色", notes="根据请求参数的roleId来指定删除角色")
    @ApiImplicitParam(name = "roleId", value = "角色主键ID", required = true, dataType = "String")
    @DeleteMapping("role/{roleId}")
    public ResultSets<String> delete(@PathVariable String roleId)
    {
        roleService.delete(roleId);
        return ResultBuilder.success(ResultCode.SUCCESS);
    }

}
