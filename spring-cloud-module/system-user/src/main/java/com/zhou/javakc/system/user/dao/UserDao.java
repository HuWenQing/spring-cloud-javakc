package com.zhou.javakc.system.user.dao;

import com.zhou.javakc.common.entity.system.User;
import com.zhou.javakc.common.jpa.base.BaseDao;
import org.springframework.data.jpa.repository.Query;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-08 15:50
 */
public interface UserDao extends BaseDao<User, String> {

    @Query(" select count(u) from User u where u.loginName = ?1 ")
    public int findByLoginName(String loginName);

}
