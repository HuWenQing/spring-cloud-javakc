package com.zhou.javakc.system.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-07 13:59
 */
@SpringBootApplication
@EnableEurekaClient
@EntityScan("com.zhou.javakc.common.entity.*")
public class SystemUser2Application {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","prod");
        SpringApplication.run(SystemUser2Application.class, args);
    }

}
