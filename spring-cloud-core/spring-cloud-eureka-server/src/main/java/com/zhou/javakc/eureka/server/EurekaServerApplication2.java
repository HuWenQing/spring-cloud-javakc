package com.zhou.javakc.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 微服务服务注册与发现
 * @project javakc
 * @author zhou
 * @date 2019年1月18日
 * @copyright Copyright (c) 2019, www.javakc.com All Rights Reserved.
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication2 {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","server2");
        SpringApplication.run(EurekaServerApplication2.class, args);
    }

}
