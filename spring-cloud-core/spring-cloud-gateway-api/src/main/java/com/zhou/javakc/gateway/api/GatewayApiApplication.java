package com.zhou.javakc.gateway.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-09 11:30
 */
@SpringBootApplication
@EnableEurekaClient
@EnableHystrix
public class GatewayApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApiApplication.class, args);
    }

//    @Bean
//    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
//        return builder.routes()
//                //basic proxy
//                .route(r -> r.path("/baidu")
//                    .uri("http://baidu.com:80/")
//                )
//                .route(r -> r.path("/sina")
//                    .uri("http://sina.com.cn/")
//                ).build();
//    }

}
