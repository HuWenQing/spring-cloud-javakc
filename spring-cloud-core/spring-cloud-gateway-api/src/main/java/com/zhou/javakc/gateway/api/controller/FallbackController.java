package com.zhou.javakc.gateway.api.controller;

import com.zhou.javakc.common.entity.base.restful.ResultBuilder;
import com.zhou.javakc.common.entity.base.restful.ResultCode;
import com.zhou.javakc.common.entity.base.restful.ResultSets;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-09 16:29
 */
@RestController
@RequestMapping("/system")
public class FallbackController {

    @GetMapping("/fallback")
    public ResultSets<String> booksFallback(){
        return ResultBuilder.error(ResultCode.INTERFACE_REQUEST_TIMEOUT);
    }

}
