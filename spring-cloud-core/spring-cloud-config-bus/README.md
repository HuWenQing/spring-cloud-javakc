rabbitMQ是一个在AMQP协议标准基础上完整的，可服用的企业消息系统。它遵循Mozilla Public License开源协议，采用 Erlang 实现的工业级的消息队列(MQ)服务器，Rabbit MQ 是建立在Erlang OTP平台上。
#####1.安装Erlang
    1.1.设置环境变量，新建ERLANG_HOME
    1.2.修改环境变量path，%ERLANG_HOME%\bin;
    1.3.cmd erl
#####2.安装rabbitmq
    2.1:设置环境变量，新建RABBITMQ_SERVER
    2.2:修改环境变量path，%RABBITMQ_SERVER%\sbin;
    2.3:cmd rabbitmqctl status
        出错安装插件: rabbitmq-plugins.bat enable rabbitmq_management
    2.4:
        cmd rabbitmq-server.bat 前台运行
        cmd rabbitmq-server.bat -detached 后台运行
        cmd cd ~rabbitmq\sbin 目录执行(rabbitmq-service.bat install) 设置开机自启
    2.5:浏览器访问http://localhost:15672
        账号:guest
        密码:guest
    2.6:cmd rabbitmqctl status
        
