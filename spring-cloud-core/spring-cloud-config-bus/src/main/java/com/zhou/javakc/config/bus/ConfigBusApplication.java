package com.zhou.javakc.config.bus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author zhou
 * @version v0.0.1
 * @date 2019-08-07 17:05
 */
@SpringBootApplication
@EnableEurekaClient
public class ConfigBusApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigBusApplication.class, args);
    }

}
