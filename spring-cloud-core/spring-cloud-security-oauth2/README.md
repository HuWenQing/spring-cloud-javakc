#生成秘钥命令:
    keytool -genkeypair -alias javakc-jwt -validity 3650 -keyalg RSA -dname "CN=jwt,OU=jtw,O=jtw,L=zurich,S=zurich,C=CH" -keypass 密码 -keystore javakc-jwt.jks -storepass 密码
#查看秘钥命令:
    keytool -list -rfc --keystore javakc-jwt.jks | openssl x509 -inform pem -pubkey
